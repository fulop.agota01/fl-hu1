const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');


// Create file tree

rootNode.classList.add('root');

function createTree(dataArr,rootN) {
  let rootList = document.createElement('UL');
  rootN.appendChild(rootList);
  if(!rootN.classList.contains('root')) {
    rootList.classList.add('nested');
  }
  
  if(dataArr === null) {
    let element = document.createElement('LI');
    let node = document.createTextNode('Folder is empty');
    element.appendChild(node);
    rootList.appendChild(element);
  } else {
    for (let i = 0; i < dataArr.length; i++){
      
      let element = document.createElement('LI');
      rootList.appendChild(element);

      let div = document.createElement('DIV');
      div.classList.add('list-item');
      element.appendChild(div);

      let sp = document.createElement('SPAN');
      div.appendChild(sp);

      let node = document.createTextNode(dataArr[i]['title']);
      sp.appendChild(node);
      
      try {
        let child = dataArr[i]['children'];
        if(child !== undefined){
          sp.parentElement.classList.add('folder');
          let icon = document.createElement('i');
          icon.classList.add('material-icons');
          let type = document.createTextNode('folder');
          icon.appendChild(type);
          div.insertBefore(icon,sp);
          createTree(child,element);
        } else {
          let icon = document.createElement('i');
          icon.classList.add('material-icons');
          let type = document.createTextNode('insert_drive_file');
          icon.appendChild(type);
          div.insertBefore(icon,sp);
        }
        
      } catch(e) {
        console.log(e);
      }
      
    }
  }
}

createTree(data,rootNode);


// Toggle folder views

let toggler = document.getElementsByClassName('folder');
let i;

for (i = 0; i < toggler.length; i++) {
  toggler[i].addEventListener('click', function() {
    this.parentElement.querySelector('.nested').classList.toggle('active');
    if (this.firstChild.innerHTML === 'folder'){
      this.firstChild.innerHTML = 'folder_open';
    } else {
      this.firstChild.innerHTML = 'folder';
    }
  });
}


/// Create context menu

let myMenu = document.createElement('DIV');
myMenu.classList.add('menu');
rootNode.appendChild(myMenu);

let options = document.createElement('UL');
options.classList.add('menu-options');
myMenu.appendChild(options);

let rename = document.createElement('LI');
rename.classList.add('menu-option');
let nodeRename = document.createTextNode('Rename');
rename.appendChild(nodeRename);
options.appendChild(rename);

let del = document.createElement('LI');
del.classList.add('menu-option');
let nodeDel = document.createTextNode('Delete');
del.appendChild(nodeDel);
options.appendChild(del);

window.addEventListener('contextmenu', e => {
  e.preventDefault();
});

const menu = document.querySelector('.menu');
const option = document.querySelectorAll('.menu-option')
let selected = rootNode;

let menuVisible = false;
let menuDisabled = true;

const toggleMenu = command => {
  menu.style.display = command === 'show' ? 'block' : 'none';
  menuVisible = !menuVisible;

};

const setPosition = ({ top, left }) => {
  menu.style.left = `${left}px`;
  menu.style.top = `${top}px`;
  toggleMenu('show');
};

window.addEventListener('click', function() {
  if(menuVisible) { 
    toggleMenu('hide'); 
  }
});


window.addEventListener('contextmenu', e => {
  const targetElement = e.target;
  if (targetElement.nodeName === 'SPAN') {
    option[0].classList.remove('option-disable');
    option[1].classList.remove('option-disable');
    selected = targetElement;
    menuDisabled = false;
  } else {
    option[0].classList.add('option-disable');
    option[1].classList.add('option-disable');
    menuDisabled = true;
  }
  console.log(targetElement.nodeName);
  e.preventDefault();
  const origin = {
    left: e.pageX,
    top: e.pageY
  };
  
  setPosition(origin);
  return false;
});

rename.addEventListener('click', function() {
  if(!menuDisabled) {
    selected.style.display = 'none';
    let fileName = selected.innerHTML;
    let inputField = document.createElement('input');
    inputField.setAttribute('value',fileName);
    selected.parentNode.appendChild(inputField);
    let dotPlace = selected.innerHTML.lastIndexOf('.');
    inputField.focus();
    inputField.setSelectionRange(0, dotPlace);
    
    inputField.addEventListener('keyup', function (event) {
      if (event.key === 'Enter') {
          console.log('Enter key pressed!!!!!');
          let x = inputField.value;
          selected.innerHTML = x;
          inputField.parentNode.removeChild(inputField);
          selected.style.display = 'inline';
      }
    });
  }
});

del.addEventListener('click', function() {
  if(!menuDisabled){
    let item = selected.parentElement; // div
    let parent = item.parentElement; // li
    let grandParent = parent.parentElement; // ul
    
    while (parent.firstChild) {
      //The list is LIVE so it will re-index each call
      parent.removeChild(parent.firstChild);
    }

    grandParent.removeChild(parent);

    if(grandParent.childNodes.length === 0) {
      let element = document.createElement('LI');
      let node = document.createTextNode('Folder is empty');
      element.appendChild(node);
      grandParent.appendChild(element);
    }
  }
  
});
