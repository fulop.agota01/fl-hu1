// Task 1
console.log('Task 1');
function getAge(birthDate) {
    const dateNow = new Date();
    let diff = dateNow.getFullYear()-birthDate.getFullYear();
    if (birthDate.getMonth() > dateNow.getMonth()){
        diff -= 1;
    } else if(birthDate.getMonth() === dateNow.getMonth() 
    && birthDate.getDate() > dateNow.getDate()){
        diff -= 1;
    }
    return diff;
}
const birthday1001 = new Date(2000, 9, 1);
const birthday0223 = new Date(2000, 1, 23);
const birthday0406 = new Date(2000, 3, 6);
const birthday0405 = new Date(2000, 3, 5);
console.log(getAge(birthday1001)); // 20
console.log(getAge(birthday0223)); // 21
console.log(getAge(birthday0406)); // 20
console.log(getAge(birthday0405)); // 21

// Task 2
console.log('\nTask 2');
function getWeekDay(date){
    const myDate = new Date(date);
    const day = myDate.getDay();
    switch(day) {
        case 0:
            return 'Sunday';
        case 1:
            return 'Monday';
        case 2:
            return 'Tuesday';
        case 3:
            return 'Wednesday';
        case 4:
            return 'Thursday';
        case 5:
            return 'Friday';
        case 6:
            return 'Saturday';
        default: 
            return 'Some day';
    }
}
console.log(getWeekDay(Date.now()));
console.log(getWeekDay(new Date(2020, 9, 22))); // "Thursday"

// Task 3
console.log('\nTask 3');
function getProgrammersDay(year) {
    if (year%4 === 0) { // in leap years
        let programmersDayDate = new Date(year, 8, 12);
        return '12 Sep, '+year+' ('+getWeekDay(programmersDayDate)+')';
    } else {
        let programmersDayDate = new Date(year, 8, 13);
        return '13 Sep, '+year+' ('+getWeekDay(programmersDayDate)+')';
    }
}
console.log(getProgrammersDay(2020)); // "12 Sep, 2020 (Saturday)"
console.log(getProgrammersDay(2019)); // "13 Sep, 2019 (Friday)"

// Task 4
console.log('\nTask 4');
function howFarIs(day) {
    const today = new Date();
    const dayCapitalized = day.charAt(0).toUpperCase() + day.slice(1);
    const dayDictionary = {
        'Sunday':0,
        'Monday':1,
        'Tuesday':2,
        'Wednesday':3,
        'Thursday':4,
        'Friday':5,
        'Saturday':6
    };
    if (getWeekDay(today) === dayCapitalized) {
        return 'Hey, today is ' + dayCapitalized + ' =)';
    } else {
        const dayNumber = dayDictionary[dayCapitalized];
        const todayNumber = today.getDay();
        let diff = dayNumber - todayNumber;
        if (diff < 0) {
            diff += 7;
        } 
        return 'It\'s ' + `${ diff }` + ' day(s) left till ' + dayCapitalized;
    }
}

console.log(howFarIs('friday')); // "It's 1 day(s) left till Friday." (on October 22nd)
console.log(howFarIs('Tuesday')); // "Hey, today is Thursday =)" (on October 22nd)

// Task 5
console.log('\nTask 5');
function isValidIdentifier(id) {
    const regx = /^\D[\w$]*$/g;
    return regx.test(id);
}
console.log(isValidIdentifier('myVar!')); // false
console.log(isValidIdentifier('myVar$')); // true
console.log(isValidIdentifier('myVar_1')); // true
console.log(isValidIdentifier('1_myVar')); // false

// Task 6
console.log('\nTask 6');
String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}
function capitalize(str){
    // Firts method - Using regex, but gives eslint error: 
    // Parsing error: Invalid regular expression: /^[a-z]|(?<=\s)[a-z]/: Invalid group
    // - Eslint doesn't recognize lookbehind assertion
    /*
    let idx = str.search(/^[a-z]|(?<=\s)[a-z]/g);  // string starts with lowecase letter or a lowercase
    while (idx != -1) {                            // letter is preceded by whitespace (lookbehind assertion)
        let capitalChar = str.charAt(idx).toUpperCase();  // search() returns -1 in case of no match
        str = str.replaceAt(idx,capitalChar);
        idx = str.search(/(?<=\s)[a-z]/g); 
    }
    return str;
    */
   
   // Second method - no regex, but also no error
   // Get all words from the sentence into an array
   let allWords = str.split(' ');
   let capWords = [];
   allWords.forEach(element => {
       let capitalChar = element.charAt(0).toUpperCase();
       element = element.replaceAt(0,capitalChar);
       capWords.push(element);
   });
   return capWords.join(' ');
}
const testStr = 'my name is John Smith. I am 27.';
console.log(capitalize(testStr)); // "My Name Is John Smith. I Am 27."

// Task 7
console.log('\nTask 7');
function isValidAudioFile(fileName){
    const regx = /^[a-zA-Z]+(\.mp3|\.flac|\.alac|\.aac)$/g;
    return regx.test(fileName);
}
console.log(isValidAudioFile('file.mp4')); // false
console.log(isValidAudioFile('my_file.mp3')); // false
console.log(isValidAudioFile('file.mp3')); // true

// Task 8
console.log('\nTask 8');
function getHexadecimalColors(str) {
    const regx = /#[a-zA-z0-9]{3}(?![a-zA-z0-9])|#[a-zA-z0-9]{6}(?![a-zA-z0-9])/g;
    let arr = str.match(regx);
    if (arr === null) {
        arr = [];
    }
    return arr;
}
const testString = 'color: #3f3; background-color: #AA00ef; and: #abcd';
console.log(getHexadecimalColors(testString)); // ["#3f3", "#AA00ef"]
console.log(getHexadecimalColors('red and #0000')); // [];

// Task 9
console.log('\nTask 9');
function isValidPassword(psw) {
    const regxUpper = /[A-Z]+/g;
    const regxLower = /[a-z]+/g;
    const regxNumber = /[0-9]+/g;
    const regxLength = /.{8,}/g;
    return regxUpper.test(psw) && regxLower.test(psw) && regxNumber.test(psw) && regxLength.test(psw);
}

console.log(isValidPassword('agent007')); // false (no uppercase letter)
console.log(isValidPassword('AGENT007')); // false (no lowercase letter)
console.log(isValidPassword('AgentOOO')); // false (no numbers)
console.log(isValidPassword('Age_007')); // false (too short)
console.log(isValidPassword('Agent007')); // true

// Task 10
console.log('\nTask 10');
function addThousandsSeparators(input){
    input = String(input);
    let inputLength = input.length;
    while (inputLength > 3){
        const stickyRegx = /\d{3}/y; // using sticky flag on regex
        let result = '';
        while(inputLength > 3){
            stickyRegx.lastIndex = inputLength-3;
            inputLength -= 3; 
            result = input.match(stickyRegx)[0] + ',' + result ;
        }
        return input.substr(0,inputLength) +','+ result.substr(0,result.length-1);
    }
    return input;
}

console.log(addThousandsSeparators('1234567890')); // "1,234,567,890"
console.log(addThousandsSeparators(1234567890)); // "1,234,567,890"
console.log(addThousandsSeparators(123)); // "123"
console.log(addThousandsSeparators(12345)); // "12,345"