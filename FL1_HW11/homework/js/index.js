function visitLink(path) {
	if(!localStorage.getItem(path)) {        // Create new element in local storage if doesn't exist already 
		localStorage.setItem(path, '1');
	  } else {                               // Otherwise update the value
		let currClicks = Number(localStorage.getItem(path));
		currClicks++;
		localStorage.setItem(path, String(currClicks));
	  }
}

function viewResults() {
	const numberOfLinks = localStorage.length;
	var mylist = document.createElement('ul');        // Create unordered list
	var element = document.getElementById('content'); // Connect it to id='content' div
	element.appendChild(mylist);
	for(let i = 0; i< numberOfLinks; i++) {
		result = 'You visited '+localStorage.key(i)+' '+localStorage.getItem(localStorage.key(i))+' time(s)';
		var element = document.createElement('li');
		var node = document.createTextNode(result);
		element.appendChild(node);
		mylist.appendChild(element);
	}
	localStorage.clear();
}
