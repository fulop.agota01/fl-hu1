// Calculate simple mathematical expressions given in infix format 

// Using regex to validate input format
    // Valid operators: +,-,/,*,^
    // Using only non-negative numbers
    // Numbers can be floats like .2 12.34
    // Whitespace is allowed between elements
const regex = /^\s*(\d*\.)?\d+\s*[\+\-\*\/\^]\s*(\d*\.)?\d+\s*([\+\-\*\/\^]\s*(\d*\.)?\d+\s*)*=?\s*$/g;   

while(true){                                // Error handling using try..catch
    try{
        let expression = prompt('Please enter a mathematical expression','');
        result = calculate(expression);
        alert('The result is: ' + result);  // Alert the result for the user
        break;                              // Stop when no error occurs 
    } 
    catch(err) {
        console.error(err);
        alert(err);
        continue;         // Stay in the loop in case of error
    }
}

function calculate(expr) {
    if(!regex.test(expr)) throw 'Incorrect input!';    // Custom error
    console.log('Input: '+expr);
    let operands = [...expr.match(/(\d*\.)?\d+/g)];            // Using regex for finding operands and operators
    const operators = [...expr.match(/[\+\-\*\/\^]/g)];
    console.debug(operands);
    console.debug(operators);

    for(let i=0; i<operators.length;i++){          // Carry out multiplications and divisions first,
        if(operators[i] === '*') {                 // update operands[] with partial results
            operands[i] = (Number(operands[i]) * Number(operands[i+1]));
            operators.splice(i,1); 
            operands.splice(i+1,1);
            i--;                                   // Increment index because we removed an element
        } else if(operators[i] === '/'){
            operands[i] = (Number(operands[i]) / Number(operands[i+1]));
            operators.splice(i,1);
            operands.splice(i+1,1); 
            i--;
        } else if(operators[i] === '^'){
            operands[i] = (Number(operands[i]) ** Number(operands[i+1]));
            operators.splice(i,1);
            operands.splice(i+1,1); 
            i--;
        }
    }
    let result = Number(operands[0]);              // Initalize result with first operand
    for(let i = 0; i < operators.length; i++){     // then carry out rest of the operations (+,-)
        if(operators[i] === '+'){
            result += Number(operands[i+1]);
        }
        else if(operators[i] === '-'){
            result -= Number(operands[i+1]);
        }
    }
    if(isNaN(result)) throw 'Result is not a number.';  // Custom error
    console.log('Result:' +result);
    return result;
}

