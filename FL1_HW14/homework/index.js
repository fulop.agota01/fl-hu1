let contentIsShown = false;
let currentTemp;
/* START TASK 1: Your code goes here */
function showContent1() {
    if(contentIsShown){
        currentTemp.parentNode.removeChild(currentTemp);
    }
    contentIsShown = true;
    let temp = document.getElementsByTagName('template')[0];
    let clon = temp.content.cloneNode(true);
    document.body.appendChild(clon);
    currentTemp = temp;
    
    let cells = document.getElementsByClassName('cell');
    for(let i = 0; i<cells.length; i++){
        cells[i].addEventListener('click', function(){
            console.log('cell '+i+' is clicked');
            if(cells[i].classList.contains('special-cell')){
                console.log('special cell clicked');
                for(let j = 0; j<cells.length; j++){
                    if(!cells[j].classList.contains('yellow') || !cells[j].classList.contains('blue')){
                        cells[j].classList.add('green');
                    }
                }
            }else if(cells[i].classList.contains('col-1')){
                console.log('col-1 cell clicked');
                for(let k = i; k < i+3; k++){
                    if(!cells[k].classList.contains('yellow')){
                        cells[k].classList.add('blue');
                        console.log('blue added?')
                    } else {
                        console.log('in else')
                    }
                }
            } else {
                cells[i].classList.add('yellow');
            }
        })
    }
}
/* END TASK 1 */

/* START TASK 2: Your code goes here */
function showContent2() {
    let temp = document.getElementsByTagName('template')[1];
    let clon = temp.content.cloneNode(true);
    document.body.appendChild(clon);
    const form = document.getElementById('form');
    const notif = document.getElementById('notif');
    const phone = document.getElementById('phone-number');
    const submitButton = document.getElementById('send-btn');

    phone.addEventListener('input', function() {
        if (!phone.checkValidity()) {
            notif.innerHTML = 'Typed number does not follow format +380*********';
            notif.classList.add('invalid');
            phone.style.borderColor = 'red';
        } else {
            submitButton.removeAttribute('disabled');
            phone.style.borderColor = 'black';
            notif.style.display = 'none';
        }
    });

    form.onsubmit = submit;

    function submit(event) {
    notif.innerHTML = 'Data was successfully sent.'
    notif.classList.add('valid');
    notif.style.display = 'block';
    phone.style.borderColor = 'black';

    event.preventDefault();
    }
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */
function showContent3() {
    let temp = document.getElementsByTagName('template')[2];
    let clon = temp.content.cloneNode(true);
    document.body.appendChild(clon);
    const container = document.querySelector('.court');
    container.addEventListener('click', function (event) {
    let x = event.clientX-20;
    let y = event.clientY-20;
    const ball = document.querySelector('.ball');
    ball.style.position = 'absolute';
    ball.style.left = `${x}px`;
    ball.style.top = `${y}px`;
    })
    const hoopA = document.getElementById('hoopA');
    const hoopB = document.getElementById('hoopB');
    const notification = document.getElementById('notification');

    hoopB.addEventListener('click', function() {
        const scoreA = document.getElementById('score-A');
        const pos = scoreA.innerHTML.indexOf(':');
        const len = scoreA.innerHTML.length;
        let score = `${scoreA.innerHTML.substr(pos+1,len-pos)}`;
        score++;
        console.log(score);
        scoreA.innerHTML = 'Team A:'+score;
        
        notification.innerHTML = 'Team A Scores!';
        notification.style.color = 'blue';
        notification.style.display = 'block';
        
        setTimeout(function(){
            notification.style.display = 'none'; 
        }, 3000);
    });

    hoopA.addEventListener('click', function() {
        const scoreB = document.getElementById('score-B');
        const pos = scoreB.innerHTML.indexOf(':');
        const len = scoreB.innerHTML.length;
        let score = `${scoreB.innerHTML.substr(pos+1,len-pos)}`;
        score++;
        console.log(score);
        scoreB.innerHTML = 'Team B:'+score;

        notification.innerHTML = 'Team B Scores!';
        notification.style.color = 'red';
        notification.style.display = 'block';
        
        setTimeout(function(){
            notification.style.display = 'none'; 
        }, 3000);
    });
}


/* END TASK 3 */
