function validInput(str) {
    const noWhitespacePattern = /\S/g;
    const result = str.match(noWhitespacePattern);
    return str.length > 0 && result !== null && result.length === str.length;
}
function findMiddle(myString) {
    if (validInput(myString)) {
        const odd = myString.length % 2;
        const pos = myString.length / 2;
        if (odd) {
            window.alert(myString.charAt(pos));
        } else {
            if (myString.charAt(pos-1) === myString.charAt(pos)) {
                window.alert('Middle characters are the same');
            } else {
                window.alert(myString.substr(pos-1,2));
            }
        }
    } else {
        window.alert('Invalid value');
    }
}
const word = window.prompt('Type in a word','');
findMiddle(word);
