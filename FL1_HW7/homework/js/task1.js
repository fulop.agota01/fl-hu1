function calc(batteries, rate) {
    let defects = batteries * rate / 100;
    if (defects % 1 !== 0) {
        defects = parseFloat(defects).toFixed(2);
    } 
    const working = batteries - defects;
    window.alert('Amount of batteries: '+batteries
    +'\nDefective rate: '+rate+'%\nAmount of defective batteries: '
    +defects+'\nAmount of working batteries: '+working);
}
const amountOfBatteries = Number(window.prompt('Type the amount of batteries', ''));
const defectiveBatteries = Number(window.prompt('Type the percentage of defective batteries', ''));
const result = isNaN(amountOfBatteries) || isNaN(defectiveBatteries) || amountOfBatteries < 0 || 
    defectiveBatteries < 0 || defectiveBatteries > 100;
result ? window.alert('Invalid input data') : calc(amountOfBatteries,defectiveBatteries) ;