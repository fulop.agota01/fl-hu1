// TASK 1
console.log('TASK 1');
function convert() {
    let i; let convertedArray = [];
    for(i=0; i<arguments.length; i++){
        switch (typeof arguments[i]) {
            case 'number':
                convertedArray.push( `${arguments[i]}` );
                break;
            case 'string':
                convertedArray.push(parseInt(arguments[i],10));
                break;
            default:
                convertedArray.push(arguments[i]);
        }
    }
    return convertedArray;
}
let result = convert('1', 2, 3, '4');
console.log(result);

// TASK 2
console.log('TASK 2');
function executeforEach(arr, myfunction) {
    //arr.forEach(el => myfunction(el));
    let i;
    for(i=0;i<arr.length;i++){
        myfunction(arr[i]);
    }
}
executeforEach([1,2,3], function(el) { 
    console.log(el * 2) 
}) // 2 4 6

// TASK 3
console.log('TASK 3');
function mapArray(arr, myfunction) {
    let resultArray = []
    executeforEach(arr, function(el) {
        if(typeof el === 'string') {
            el = parseInt(el,10);
        }
        resultArray.push(myfunction(el));
    })
    return resultArray; 
}
result = mapArray([2, '5', 8], function(el) {
    return el + 3
});
console.log(result)

// TASK 4
console.log('TASK 4');
function filterArray(arr, myfunction) {
    //return arr.filter(el => myfunction(el));
    let res= [];
    executeforEach(arr, function(el){
        if(myfunction(el)) {
            res.push(el);
        }
    });
    return res;
}
result = filterArray([2, 5, 8], function(el) { 
    return el % 2 === 0 
})
console.log(result)
// TASK 5
console.log('TASK 5')
function getValuePosition(arr, el){
    let i;
    let found = false;
    for(i=0;i<arr.length;i++){
        if(arr[i]===el){
            found = true;
            return i+1
        }
    }
    if(!found){
        return false
    }
}

console.log(getValuePosition([2, 5, 8], 8)) // returns 3
console.log(getValuePosition([12, 4, 6], 1)) // returns false

// TASK 6
console.log('Task 6')
function flipOver(str) {
    let i=str.length-1;
    result = '';
    for(i;i>=0;i--){
        result+=str[i];
    }
    return result
}
console.log(flipOver('hey world')) // 'dlrow yeh'
// TASK 7
console.log('TASK 7')
//7. Write a function which creates an array from the given range of numbers
function makeListFromRange(arr) {
    let i = arr[0];
    result = [];
    for(i;i<=arr[1];i++){
        result.push(i);
    } 
    return result;
}
console.log(makeListFromRange([2, 7])) // [2, 3, 4, 5, 6, 7]
// TASK 8
console.log('TASK 8')
function getArrayOfKeys(arr,key) {
    let result = [];
    executeforEach(arr,function(el){
        result.push(el[key])
    })
    return result;
}

const fruits = [
 { name: 'apple', weight: 0.5 },
 { name: 'pineapple', weight: 2 }
];
console.log(getArrayOfKeys(fruits, 'name')); 
// returns [‘apple’, ‘pineapple’]

// TASK 9 
console.log('TASK 9')
function getTotalWeight(arr) {
    let total = 0;
    executeforEach(arr,function(el){
        total += el['weight'];
    })
    return total
}
const basket = [
 { name: 'Bread', weight: 0.3 },
 { name: 'Coca-Cola', weight: 0.5 },
 { name: 'Watermelon', weight: 8 }
];
console.log(getTotalWeight(basket));
// returns 8.8

// TASK 10
console.log('TASK 10')

function getPastDay(date, days) {
    let newDate = new Date(date);
    newDate.setDate(newDate.getDate() - days);
    return newDate.getDate();
}

const date = new Date(2020, 0, 2);
console.log(getPastDay(date, 1)); // 1, (1 Jan 2020)
console.log(getPastDay(date, 2)); // 31, (31 Dec 2019)
console.log(getPastDay(date, 365)); // 2, (2 Jan 2019)

// TASK 11
console.log('TASK 11')
// "YYYY/MM/DD HH:mm"
function formatDate(date) {
    //console.log(date.toLocaleString('en-GB').slice(0,17));  //using String.slice is forbidden
    let year = `${date.getFullYear()}`;
    let month = `${date.getMonth() + 1}`;
    let day = `${date.getDate()}`;
    let hour = `${date.getHours()}`;
    let minute = `${date.getMinutes()}`;

    if(month.length === 1){
        month = '0'+month;
    }
    if(day.length === 1){
        day = '0'+day;
    }
    if(hour.length === 1){
        hour = '0'+hour;
    }
    if(minute.length === 1){
        minute = '0'+minute;
    }

    let formattedDate = year+'/'+month+'/'+day+' '+hour+':'+minute;
    return formattedDate;

    
}
console.log(formatDate(new Date('6/15/2019 09:15:00'))); // "2019/06/15 09:15"
console.log(formatDate(new Date())); // "2020/04/07 12:56" // gets current local time