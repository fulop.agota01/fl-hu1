/*global questions*/

document.getElementById('btn-start').addEventListener('click', start);
document.getElementById('btn-skip').addEventListener('click', skip);
document.getElementById('btn-0').addEventListener('click', answer0);
document.getElementById('btn-1').addEventListener('click', answer1);
document.getElementById('btn-2').addEventListener('click', answer2);
document.getElementById('btn-3').addEventListener('click', answer3);

let myGame;
let myQuestions;
let gameDiv = document.getElementById('game-interface');
let btnSkip = document.getElementById('btn-skip');
const startingPrize = 100;
const finalPrize = 1000000;

// Fisher–Yates shuffle
function _shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
}

class Game {
    constructor(arr) {
        this.pTotal = 0;
        this.pCurr = startingPrize;
        this.questionsArr = arr;
        this.round = 0;
        this.skipped = false;
        this.btnSkip = document.getElementById('btn-skip');
        this.questionTitle = document.getElementById('question-title');
        this.btn0 = document.getElementById('btn-0');
        this.btn1 = document.getElementById('btn-1');
        this.btn2 = document.getElementById('btn-2');
        this.btn3 = document.getElementById('btn-3');
        this.prizeTotalStr = document.getElementById('prize-total');
        this.prizeCurrentStr = document.getElementById('prize-current');
        this.updateQuestion();
    }
    updateQuestion() {
        this.currQuestion = myQuestions[this.round].question;
        this.answer0 = myQuestions[this.round].content[0];
        this.answer1 = myQuestions[this.round].content[1];
        this.answer2 = myQuestions[this.round].content[2];
        this.answer3 = myQuestions[this.round].content[3];
        this.questionTitle.innerHTML = this.currQuestion;
        this.btn0.innerHTML = this.answer0;
        this.btn1.innerHTML = this.answer1;
        this.btn2.innerHTML = this.answer2;
        this.btn3.innerHTML = this.answer3;
        this.prizeTotalStr.innerHTML = 'Total prize: '+this.pTotal;
        this.prizeCurrentStr.innerHTML = 'Prize on current round: '+this.pCurr;
    }
}

function skip() {
    document.getElementById('btn-skip').disabled = true;
    myGame.round++;
    myGame.updateQuestion();
}

function start() {
    _shuffle(questions);
    //filtering for unique questions
    myQuestions = questions.filter((value,index,self) => 
        self.findIndex(t => t.question === value.question)===index);
    myGame = new Game(myQuestions);
    gameDiv.style.display = 'block';
    btnSkip.style.display = 'block';
    btnSkip.disabled = false;
}

function answer0() {
    if(myGame.questionsArr[myGame.round].correct === 0){
        myGame.pTotal += myGame.pCurr;
        if(myGame.pTotal >= finalPrize){
            window.alert('Congratulations! You won: '+myGame.pTotal);
            clear();
        } else {
            myGame.pCurr *= 2;
            myGame.round++;
            myGame.updateQuestion();
        }
    } else {
        window.alert('Game over. Your prize is: '+ myGame.pTotal);
        clear();
    } 
}

function answer1() {
    if(myGame.questionsArr[myGame.round].correct === 1){
        myGame.pTotal += myGame.pCurr;
        if(myGame.pTotal >= finalPrize){
            window.alert('Congratulations! You won: '+ myGame.pTotal);
            clear();
        } else {
            myGame.pCurr *= 2;
            myGame.round++;
            myGame.updateQuestion();
        }
    } else {
        window.alert('Game over. Your prize is: '+ myGame.pTotal);
        clear();
    } 
}
function answer2() {
    if(myGame.questionsArr[myGame.round].correct === 2){
        myGame.pTotal += myGame.pCurr;
        if(myGame.pTotal >= finalPrize){
            window.alert('Congratulations! You won: '+myGame.pTotal);
            clear();
        } else {
            myGame.pCurr *= 2;
            myGame.round++;
            myGame.updateQuestion();
        }
    } else {
        window.alert('Game over. Your prize is: '+ myGame.pTotal);
        clear();
    } 
}
function answer3() {
    if(myGame.questionsArr[myGame.round].correct === 3){
        myGame.pTotal += myGame.pCurr;
        if(myGame.pTotal >= finalPrize){
            window.alert('Congratulations! You won: '+myGame.pTotal);
            clear();
        } else {
            myGame.pCurr *= 2;
            myGame.round++;
            myGame.updateQuestion();
        }
    } else {
        window.alert('Game over. Your prize is: '+ myGame.pTotal);
        clear();
    } 
}

function clear(){
    myGame = null;
    gameDiv.style.display = 'none';
    btnSkip.style.display = 'none';
}
