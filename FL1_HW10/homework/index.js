// TASK 1
console.log('TASK 1');
function isEquals(arg1, arg2) {
    return arg1 === arg2;
}
console.log(isEquals(3, '3')); // => false

// TASK 2
console.log('TASK 2');
function numberToString(num){
    return String(num);
}
console.log(numberToString(1258)); // => ‘1258’

// TASK 3
console.log('TASK 3');
function storeNames() {
    let names = [];
    for(let i = 0; i<arguments.length;i++){
        names.push(arguments[i]);
    }
    return names;
}
console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));
// => ['Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy']

// TASK 4
console.log('TASK 4');
function getDivision(num1, num2) {
    let dividend, divisor, quotient;
    if(num1 >= num2){
        dividend = num1;
        divisor = num2;
    } else {
        dividend = num2;
        divisor = num1;
    }
    quotient = dividend / divisor;
    return quotient;
}
console.log(getDivision(4, 1)); // => 4
console.log(getDivision(2, 8)); // => 4

// TASK 5
console.log('TASK 5');
function negativeCount(numArr) {
    let negCount = 0;
    numArr.forEach(element => {
        if(element < 0){
            negCount++;
        }
    });
    return negCount;
}
console.log(negativeCount([4, 3, 2, 9])); // => 0
console.log(negativeCount([0, -3, 5, 7])); // => 1

// TASK 6
console.log('TASK 6');
function letterCount(str,letter) {
    let lCount = 0;
    for(let i=0;i<str.length;i++) {
        if(str[i]===letter) {
            lCount++;
        }
    }
    return lCount;
}
console.log(letterCount('Marry', 'r')); // => 2
console.log(letterCount('Barny', 'y')); // => 1
console.log(letterCount('', 'z')); // => 0

// TASK 7
console.log('TASK 7');
function countPoints(resultsArr) {
    let result = [];
    let x, y;
    let points = 0;
    const winPoint = 3;
    const tiePoint = 1;
    resultsArr.forEach(element => {
        result = element.split(':');
        x = Number(result[0]);
        y = Number(result[1]);
        if(x > y){
            points += winPoint;
        } else if(x === y) {
            points += tiePoint;
        }
    });
    return points;
}
console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100'])) // => 17